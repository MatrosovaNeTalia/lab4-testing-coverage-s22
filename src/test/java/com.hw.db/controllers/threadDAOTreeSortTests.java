package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class threadDAOTreeSortTests {
    @Test
    @DisplayName("When passing since param, should compare dates")
    void passSinceAndSqlShouldContainComparison() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(0, 10, 20, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(0), Mockito.eq(20), Mockito.eq(10));
    }

    @Test
    @DisplayName("When passing since and descending order params, should add ordering")
    void passSinceAndDescTrueSqlShouldContainDesc() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(0, 10, 20, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(0), Mockito.eq(20), Mockito.eq(10));
    }
}
