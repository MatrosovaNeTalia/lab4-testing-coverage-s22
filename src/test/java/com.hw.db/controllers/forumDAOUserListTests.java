package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class forumDAOUserListTests {
    @Test
    @DisplayName("When passing empty values, should not add restrictions on query")
    void allOptionalParametersAreNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("abc", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.eq(new Object[]{"abc"}), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("When passing all values, should add filter, ordering and limit")
    void sinceIsNotNullAndDescIsTrueAndLimitIsNotNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("abc", 10, "123", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.eq(new Object[]{"abc", "123", 10}), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("When passing only since value, should add a WHERE clause")
    void sinceIsNotNullAndDescNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("abc", null, "123", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.eq(new Object[]{"abc", "123"}), Mockito.any(UserDAO.UserMapper.class));
    }
}
