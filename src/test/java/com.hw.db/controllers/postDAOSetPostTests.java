package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class postDAOSetPostTests {
    @Test
    @DisplayName("Should change author, message and timestamp")
    void changeAuthorMessageAndTimestamp() {
        Post post = new Post();
        post.setAuthor("author");
        post.setMessage("message");
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setAuthor("old_author");
        prev.setMessage("old_message");
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq("author"), Mockito.eq("message"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("Should change message and timestamp")
    void changeMessageAndTimestamp() {
        Post post = new Post();
        post.setMessage("message");
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setMessage("old_message");
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq("message"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("Should change timestamp")
    void changeTimestamp() {
        Post post = new Post();
        post.setCreated(new Timestamp(0));

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(prev);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.eq(new Timestamp(0)), Mockito.eq(0));
    }

    @Test
    @DisplayName("Should change nothing")
    void changeNothing() {
        Post post = new Post();
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class), Mockito.any(PostDAO.PostMapper.class), Mockito.anyInt())).thenReturn(post);

        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);

        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }
}
