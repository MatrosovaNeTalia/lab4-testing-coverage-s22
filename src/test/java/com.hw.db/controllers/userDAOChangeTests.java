package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class userDAOChangeTests {
    @Test
    @DisplayName("No changes")
    void allNull() {
        User user = new User();
        user.setNickname("nickname");
        user.setEmail("a");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    @DisplayName("Email is changed")
    void setEmail() {
        User user = new User();
        user.setNickname("nickname");
        user.setEmail("a");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.eq("a"), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Email and full name are changed")
    void setEmailAndFullName() {
        User user = new User();
        user.setNickname("nickname");
        user.setEmail("a");
        user.setFullname("b");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.eq("a"), Mockito.eq("b"), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Email and about are changed")
    void setEmailAndAbout() {
        User user = new User();
        user.setNickname("nickname");
        user.setEmail("a");
        user.setAbout("b");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq("a"), Mockito.eq("b"), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Full name and about are changed")
    void setFullnameAndAbout() {
        User user = new User();
        user.setNickname("nickname");
        user.setFullname("a");
        user.setAbout("b");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq("a"), Mockito.eq("b"), Mockito.eq(user.getNickname()));
    }

    @Test
    @DisplayName("Everything is changed")
    void setAll() {
        User user = new User();
        user.setNickname("nickname");
        user.setEmail("a");
        user.setFullname("b");
        user.setAbout("c");

        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

        new UserDAO(mockJdbc);
        UserDAO.Change(user);

        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.eq("a"), Mockito.eq("b"), Mockito.eq("c"), Mockito.eq(user.getNickname()));
    }
}
